"""
==============
chisel_methods
==============

Helper functions for invoking Chisel generators from within TheSyDeKick.
"""

import os
import sys
import subprocess
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *

# Extend thesdk for logging
class chisel_methods(thesdk):
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self): 
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 

    def run_chisel_generator(self, entity, mainclass, args):
        """ Compile and execute a Chisel generator with sbt.

            Default assumptions and behavior:

            * The entity contains a ``chisel/`` directory that contains the generator
            * Generates verilog under ``<entity.entitypath>/simulations/<timestamp>/rtl/``

            Parameters
            ----------
                entity : Instance of ``thesdk``
                    Entity that contains the Chisel generator source code

                mainclass : str
                    Fully qualified main class name of the generator.

                args : dict
                    Command line arguments to pass to the Chisel generator
                    and the underlying ``ChiselStage``

        """

        # Leave user-defined -td or --target-dir unchanged
        if not ('-td' in args.keys() or '--target-dir' in args.keys()):
            args['--target-dir'] = entity.rtlsimpath

        # Construct command string
        args_str = ' '.join([' '.join(pair) for pair in args.items()])
        chiselpath = os.path.join(entity.entitypath, 'chisel')
        chiselcmd = "cd %s && sbt 'runMain %s %s' && sync %s" \
                % (chiselpath, mainclass, args_str, entity.rtlsimpath)

        # Execute command
        self.print_log(type='I', msg='Running command: %s' % chiselcmd)
        output = subprocess.check_output(chiselcmd, shell=True)
        print(output.decode('utf-8'))

