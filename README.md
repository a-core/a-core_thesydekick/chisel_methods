# `chisel_methods`

Helper functions for calling Chisel generators from TheSyDeKick.

### [API Documentation](https://a-core.gitlab.io/a-core_thesydekick/chisel_methods/)
